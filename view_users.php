<!DOCTYPE html>
<?php
session_start();
if (!$_SESSION['admin_email']) {
  header("location: admin_login.php");
}
else {
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "php_stackskills";
  $con = mysqli_connect($servername, $username, $password, $dbname);
  // Check connection
  if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
  }
  ?>
  <?php
  if (isset($_POST['reset'])) {
    // Update the votes to 0
    $reset = "update images set markje = 0, mikhas = 0, naymal = 0";
    $run_reset = mysqli_query($con,$reset);
    // Update and set the user to not voted
    $resetvote = "update register_user set user_voted = 0";
    $run_resetvote = mysqli_query($con,$resetvote);
  }
   ?>
  <html>
  <head>
    <meta charset="utf-8">
    <title>View Users - Admin panel</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <?php
    if (isset($_SESSION['user_email'])) {
      echo "<script>userlogedin();</script>";
    }
    if (isset($_SESSION['admin_email'])){
      echo "<script>adminlogedin();</script>";
    }
     ?>
    <nav class="cyan darken-3">
      <div class="nav-wrapper">
        <a href="index.php" class="brand-logo center">Admin Panel</a>
        <a id="MobileMenu" href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a id="Usernotlog" href="login.php">Login</a></li>
          <li><a id="Userislog" href="home.php">UserHomepage</a></li>
          <li><a href="registration.php">Register</a></li>
          <li><a id="Adminnotlog" href="admin_login.php">Admin Login</a></li>
          <li><a id="Adminislog" href="view_users.php">AdminHomepage</a></li>
          <li><a href="logout.php"> Logout</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
          <li><a href="login.php">Login</a></li>
          <li><a href="registration.php">Register</a></li>
          <li><a href="admin_login.php">Admin Login</a></li>
          <li><a href="logout.php"> Logout</a></li>
        </ul>
      </div>
    </nav>
    <div class="content">
      <h3>Welcome: <?php echo $_SESSION['admin_email']; ?></h3>
      <table class="responsive-table">
        <thead>
          <tr>
            <th data-field="id">S.N</th>
            <th data-field="name">Name</th>
            <th data-field="price">Email</th>
            <th data-field="country">country</th>
            <th data-field="id">Image</th>
            <th data-field="Bday">Birthday</th>
            <th data-field="name">Reg Date</th>
            <th data-field="price">Delete</th>
          </tr>
        </thead>

        <?php
        $sel = "select * from register_user";
        $run = mysqli_query($con,$sel);
        if (!$run) {
          die("Select failed");
        }
        $i=0;
        while ($row = mysqli_fetch_array($run)) {
          $id = $row['user_id'];
          $name = $row['user_name'];
          $email = $row['user_email'];
          $country = $row['user_country'];
          $image = $row['user_image'];
          $Bday = $row['user_bdate'];
          $date = $row['register_date'];
          $i++;
          ?>
          <tbody>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo $name; ?></td>
              <td><?php echo $email; ?></td>
              <td><?php echo $country; ?></td>
              <td><img class="materialboxed" src="images/<?php echo $image;?>"  width="40" height="40" alt="profile image"></td>
              <td><?php echo $Bday; ?></td>
              <td><?php echo $date; ?></td>
              <td><a href="view_users.php?id=<?php echo $id;?>">Delete</a></td>
            </tr>
          </tbody>
          <?php } ?>
        </table>
        <?php
        if(isset($_GET['id'])) {
          $get_id = $_GET['id'];
          $delete = "delete from register_user where user_id='$get_id'";
          $run_delete = mysqli_query($con,$delete);
          if($run_delete) {
            echo "<script>alert('Delete successful')</script>";
            echo "<script>window.open('view_users.php','_self')</script>";
          }
        }
        ?>
        <?php
        // Get vote results from database
          $sel = "select * from images";
          $run = mysqli_query($con,$sel);

          $row = mysqli_fetch_array($run);

          $markje_votes = $row['markje'];
          $mikhas_votes = $row['mikhas'];
          $naymal_votes = $row['naymal'];


          $count_all = $markje_votes+$mikhas_votes+$naymal_votes;

          if ($count_all == 0) {
            $per_markje = "No voters";
            $per_mikhas = "No voters";
            $per_naymal = "No voters";
          }else {
            $per_markje = round($markje_votes*100/$count_all, 2) . "%";
            $per_mikhas = round($mikhas_votes*100/$count_all, 2) . "%";
            $per_naymal = round($naymal_votes*100/$count_all, 2) . "%";
          }
         ?>
         <div class="row">
           <p>
             Number of voters: <?php echo $count_all; ?>
           </p>
           <p>
             The votes: <?php echo $per_markje . ",   " . $per_mikhas . ",    " . $per_naymal; ?>
           </p>
         </div>
        <form class="ResetVote" action="view_users.php" method="post">
          <button class="btn waves-effect waves-light" type="submit" name="reset">Reset the current votes</button>
        </form>
      </div>
      <footer class="page-footer cyan darken-3">
        <div class="footer-copyright">
          <div class="container">
            © 2016 Copyright
            <a class="grey-text text-lighten-4 right" href="mailto:sefo1300@student.miun.se">sefo1300@student.miun.se</a>
          </div>
        </div>
      </footer>
    </body>
    </html>

    <?php

    }
    ?>
