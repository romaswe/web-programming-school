<?php
    session_start();
    // TODO: om ork finns: göra en snyggare sammanställningen av omröstningen (resultatet)
    // TODO: om ork finns: gör så man kan se på adminsidan vem som rösta på vad
    // TODO: Om ork finns: Göra så man måste begräfta innan man nollar omröstningen
    // TODO: om ork finns: göra om voten så man kan lägga upp bilder ifrån adminsidan så man kan ändra pollen
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Startpage</title>
  <link rel="shortcut icon" href="favicon.ico" />
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <?php
  if (isset($_SESSION['user_email'])) {
    echo "<script>userlogedin();</script>";
  }
  if (isset($_SESSION['admin_email'])){
    echo "<script>adminlogedin();</script>";
  }
   ?>
  <nav class="cyan darken-3">
    <div class="nav-wrapper">
      <a href="index.php" class="brand-logo center">Startpage</a>
      <a id="MobileMenu" href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a id="Usernotlog" href="login.php">Login</a></li>
        <li><a id="Userislog" href="home.php">UserHomepage</a></li>
        <li><a href="registration.php">Register</a></li>
        <li><a id="Adminnotlog" href="admin_login.php">Admin Login</a></li>
        <li><a id="Adminislog" href="view_users.php">AdminHomepage</a></li>
        <li><a href="logout.php"> Logout</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="login.php">Login</a></li>
        <li><a href="registration.php">Register</a></li>
        <li><a href="admin_login.php">Admin Login</a></li>
        <li><a href="logout.php"> Logout</a></li>
      </ul>
    </div>
  </nav>
  <div class="content valign-wrapper">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <a href="login.php">
            <div class="icon-block">
              <h2 class="center light-blue-text"><i class="material-icons">stay_primary_portrait</i></h2>
              <h5 class="center">Login</h5>
              <p class="light">
                Welcome to this site! Press this box if you want to login and se the wonders of this page.
                And remember you can always use the navigatonbar to navigate around.
              </p>
            </div>
          </a>
        </div>

        <div class="col s12 m4">
          <a href="registration.php">
            <div class="icon-block">
              <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
              <h5 class="center">Register</h5>
              <p class="light">
                Are you not yet a user on this wonderfull site dont hesitate to register today!
                And as always you an use the navigatonbar to get around.
              </p>
            </div>
          </a>
        </div>

        <div class="col s12 m4">
          <a href="admin_login.php">
            <div class="icon-block">
              <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
              <h5 class="center">Admin login</h5>
              <p class="light">
                Are you one of our first class admins on this wonderfull site? log in to se and administer the other users
              </p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <footer class="page-footer cyan darken-3">
    <div class="footer-copyright">
      <div class="container">
        © 2016 Copyright
        <a class="grey-text text-lighten-4 right" href="mailto:sefo1300@student.miun.se">sefo1300@student.miun.se</a>
      </div>
    </div>
  </footer>
</body>
</html>
