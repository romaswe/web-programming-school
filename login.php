<!DOCTYPE html>
<?php
session_start();
if (isset($_SESSION['user_email'])) {
  header("location: home.php");
}
else {
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "php_stackskills";
  $con = mysqli_connect($servername, $username, $password, $dbname);
  // Check connection
  if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
  }
  ?>
  <html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <?php
    if (isset($_SESSION['user_email'])) {
      echo "<script>userlogedin();</script>";
    }
    if (isset($_SESSION['admin_email'])){
      echo "<script>adminlogedin();</script>";
    }
     ?>
    <nav class="cyan darken-3">
      <div class="nav-wrapper">
        <a href="index.php" class="brand-logo center">Login</a>
        <a id="MobileMenu" href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a id="Usernotlog" href="login.php">Login</a></li>
          <li><a id="Userislog" href="home.php">UserHomepage</a></li>
          <li><a href="registration.php">Register</a></li>
          <li><a id="Adminnotlog" href="admin_login.php">Admin Login</a></li>
          <li><a id="Adminislog" href="view_users.php">AdminHomepage</a></li>
          <li><a href="logout.php"> Logout</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
          <li><a href="login.php">Login</a></li>
          <li><a href="registration.php">Register</a></li>
          <li><a href="admin_login.php">Admin Login</a></li>
          <li><a href="logout.php"> Logout</a></li>
        </ul>
      </div>
    </nav>
    <div class="content">
      <div class="row">
        <h2 class="center-align">Welcome and please Login to the site</h2>
        <form class="col s6 offset-s3" action="login.php" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="input-field col s12">
              <input id="user_email" type="email" name="user_email" class="validate">
              <label for="user_email">Email</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="user_pass" type="password" name="user_pass" class="validate">
              <label for="user_pass">Password</label>
            </div>
          </div>
          <button class="btn waves-effect waves-light" type="submit" name="login">Login Now
            <i class="material-icons right">send</i>
          </button>
        </form>
      </div>
      <?php
      if (isset($_POST['login'])) {
        $user_pass = md5(mysqli_real_escape_string($con,$_POST['user_pass']));
        $user_email = mysqli_real_escape_string($con,$_POST['user_email']);

        $sel = "select * from register_user where user_email = '$user_email' AND user_pass = '$user_pass'";
        $run = mysqli_query($con,$sel);
        if (!$run) {
          die("Select failed");
        }

        $check = mysqli_num_rows($run);
        if ($check ==0) {
          echo "<script>alert('Email or password in not correct')</script>";
          exit();
        }
        else {
          $_SESSION['user_email']=$user_email;
          echo "<script>window.open('home.php','_self')</script>";
        }
      }
      ?>

    </div>
    <footer class="page-footer cyan darken-3">
      <div class="footer-copyright">
        <div class="container">
          © 2016 Copyright
          <a class="grey-text text-lighten-4 right" href="mailto:sefo1300@student.miun.se">sefo1300@student.miun.se</a>
        </div>
      </div>
    </footer>
  </body>
  </html>

  <?php

  }
  ?>
