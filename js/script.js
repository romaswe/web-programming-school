$(document).ready(function() {
  var date = new Date();
  $('select').material_select();
  $('.parallax').parallax();
  $('.materialboxed').materialbox();
  $('.datepicker').pickadate({
    format: 'yyyy/mm/dd',
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 200, // Creates a dropdown of 200 years to control year
    closeOnClear: true,
    min: new Date(date.getFullYear() - 100, date.getMonth() ,date.getDate()),
    max: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
    onSet: function( arg ){
      if ( 'select' in arg ){ //prevent closing on selecting month/year
        this.close();
      }
    }

  });
  $(".button-collapse").sideNav();
  $(".nav-wrapper").click(function(){
    $('.button-collapse').sideNav('hide');
  });
  $("#Userislog").hide();
  $("#Usernotlog").show();
  $("#Adminislog").hide();
  $("#Adminnotlog").show();
});
function fillall() {
  //change header to red and ask them to fill all fields
  document.getElementById("ErrorMessage").style.color = "red";
  document.getElementById("ErrorMessage").innerHTML = "Please fill all the fields!";
}
function imagecheck() {
  //change header to red and ask them to fill all fields
  document.getElementById("ErrorMessage").style.color = "red";
  document.getElementById("ErrorMessage").innerHTML = "Sorry, only JPG and png files are allowed!";
}
function passlen() {
  //change header to red and ask them to fill all fields
  document.getElementById("ErrorMessage").style.color = "red";
  document.getElementById("ErrorMessage").innerHTML = "Please have a password longer then 3 characters!";
}
function emareg() {
  //change header to red and ask them to fill all fields
  document.getElementById("ErrorMessage").style.color = "red";
  document.getElementById("ErrorMessage").innerHTML = "this email is already registerd";
}
function usereg() {
  //change header to red and telle then that that username is alrady registerds
  document.getElementById("ErrorMessage").style.color = "red";
  document.getElementById("ErrorMessage").innerHTML = "This username is already registerd";
}
function regsuc() {
  //change header to green and tell them the Registration is done
  document.getElementById("ErrorMessage").style.color = "green";
  document.getElementById("ErrorMessage").innerHTML = "Registration successful!";
}
function userlogedin() {
  // if you are logedin you cant se the login button and it is replaced whit a homepage button insted
  $(document).ready(function() {
    $("#Userislog").show();
    $("#Usernotlog").hide();
  });
}
function adminlogedin() {
  // if you are logedin you cant se the login button and it is replaced whit a homepage button insted
  $(document).ready(function() {
    $("#Adminislog").show();
    $("#Adminnotlog").hide();
  });
}
