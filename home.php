<?php
session_start();
if (!$_SESSION['user_email']) {
  header("location: index.php");
}
else {
  $session_useremail = $_SESSION['user_email'];
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "php_stackskills";
  $con = mysqli_connect($servername, $username, $password, $dbname);
  // Check connection
  if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
  }
  ?>
  <?php
  // Get user name and image from user
  $sel_user = "select * from register_user where user_email = '$session_useremail'";
  $run_user = mysqli_query($con,$sel_user);
  if (!$run_user) {
    die("Select failed");
  }
  $row = mysqli_fetch_array($run_user);
  $name = $row['user_name'];
  $image = $row['user_image'];
  $hasvoted = $row['user_voted'];

  // increment the vote for the one thats is voted
    if (isset($_POST['markje'])) {
      if ($hasvoted == 0) {
        $markje = "update images set markje = markje+1";
        $run_markje = mysqli_query($con,$markje);
        $addvote = "update register_user set user_voted = 1 where user_email = '$session_useremail'";
        $run_addvote = mysqli_query($con,$addvote);
      }else {
        echo "<script>alert('You have alrady voted!');</script>";
      }
    }
    if (isset($_POST['mikhas'])) {
      if ($hasvoted == 0) {
        $mikhas = "update images set mikhas = mikhas+1";
        $run_mikhas = mysqli_query($con,$mikhas);
        $addvote = "update register_user set user_voted = 1 where user_email = '$session_useremail'";
        $run_addvote = mysqli_query($con,$addvote);
      }else {
        echo "<script>alert('You have alrady voted!');</script>";
      }
    }
    if (isset($_POST['naymal'])) {
      if ($hasvoted == 0) {
        $naymal = "update images set naymal = naymal+1";
        $run_naymal = mysqli_query($con,$naymal);
        $addvote = "update register_user set user_voted = 1 where user_email = '$session_useremail'";
        $run_addvote = mysqli_query($con,$addvote);
      }else {
        echo "<script>alert('You have alrady voted!');</script>";
      }
    }

  // Get vote results from database
    $sel = "select * from images";
    $run = mysqli_query($con,$sel);

    $row = mysqli_fetch_array($run);

    $markje_votes = $row['markje'];
    $mikhas_votes = $row['mikhas'];
    $naymal_votes = $row['naymal'];

    $count_all = $markje_votes+$mikhas_votes+$naymal_votes;

    if ($count_all == 0) {
      $per_markje = "No voters";
      $per_mikhas = "No voters";
      $per_naymal = "No voters";
    }else {
      $per_markje = round($markje_votes*100/$count_all, 2) . "%";
      $per_mikhas = round($mikhas_votes*100/$count_all, 2) . "%";
      $per_naymal = round($naymal_votes*100/$count_all, 2) . "%";
    }
   ?>

  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <title>HomePage</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <?php
    if (isset($_SESSION['user_email'])) {
      echo "<script>userlogedin();</script>";
    }
    if (isset($_SESSION['admin_email'])){
      echo "<script>adminlogedin();</script>";
    }
     ?>
    <nav class="cyan darken-3">
      <div class="nav-wrapper">
        <a href="index.php" class="brand-logo center">HomePage</a>
        <a id="MobileMenu" href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a id="Usernotlog" href="login.php">Login</a></li>
          <li><a id="Userislog" href="home.php">UserHomepage</a></li>
          <li><a href="registration.php">Register</a></li>
          <li><a id="Adminnotlog" href="admin_login.php">Admin Login</a></li>
          <li><a id="Adminislog" href="view_users.php">AdminHomepage</a></li>
          <li><a href="logout.php"> Logout</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
          <li><a href="login.php">Login</a></li>
          <li><a href="registration.php">Register</a></li>
          <li><a href="admin_login.php">Admin Login</a></li>
          <li><a href="logout.php"> Logout</a></li>
        </ul>
      </div>
    </nav>
    <div class="row section">
    <div class="col s6 offset-s3">
      <div class="card horizontal">
        <div class="card-image">
          <img class="materialboxed responsive-img" src="images/<?php echo $image;?>" alt="profile image">
        </div>
        <div class="card-stacked">
          <div class="card-content">
            <p>Welcome <?php echo $name; ?>!</p>
            <p>
              Select your vote! But remember you can only vote one time untill the votes are reseted
            </p>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="content valign-wrapper">
      <div class="row">
    <div class="section">
      <!--   Icon Section   -->
        <h4>Number of voters: <?php echo "$count_all";?></h4>
        <div class="col s12 m4">
            <div class="icon-block">
          <img class="" src="images_vote/markje.jpg" alt="martin" width="200" height="200" />
          <form action="home.php" method="post">
            <button class="btn waves-effect waves-light" type="submit" name="markje">Vote for markje</button>
          </form>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <?php
          echo "<p>Results: </p> $per_markje";
           ?>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
          <img class="" src="images_vote/mikhas.jpg" alt="martin" width="200" height="200" />
          <form action="home.php" method="post">
            <button class="btn waves-effect waves-light" type="submit" name="mikhas">Vote for mikhas</button>
          </form>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <?php
          echo "<p>Results: </p> $per_mikhas";
           ?>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
          <img class="" src="images_vote/naymal.jpg" alt="martin" width="200" height="200" />
          <form action="home.php" method="post">
            <button class="btn waves-effect waves-light" type="submit" name="naymal">Vote for naymal</button>
          </form>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <?php
          echo "<p>Results: </p> $per_naymal";
           ?>
            </div>
        </div>
      </div>
    </div>
  </div>
    <footer class="page-footer cyan darken-3">
      <div class="footer-copyright">
        <div class="container">
          © 2016 Copyright
          <a class="grey-text text-lighten-4 right" href="mailto:sefo1300@student.miun.se">sefo1300@student.miun.se</a>
        </div>
      </div>
    </footer>
  </body>
  </html>


  <?php
}
?>
