<!DOCTYPE html>
<?php
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "php_stackskills";
$con = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if ($con->connect_error) {
  die("Connection failed: " . $con->connect_error);
}
?>
<html>
<head>
  <meta charset="utf-8">
  <title>Registration form</title>
  <link rel="shortcut icon" href="favicon.ico" />
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <?php
  if (isset($_SESSION['user_email'])) {
    echo "<script>userlogedin();</script>";
  }
  if (isset($_SESSION['admin_email'])){
    echo "<script>adminlogedin();</script>";
  }
  ?>
  <nav class="cyan darken-3">
    <div class="nav-wrapper">
      <a href="index.php" class="brand-logo center">Registration</a>
      <a id="MobileMenu" href="index.php" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a id="Usernotlog" href="login.php">Login</a></li>
        <li><a id="Userislog" href="home.php">UserHomepage</a></li>
        <li><a href="registration.php">Register</a></li>
        <li><a id="Adminnotlog" href="admin_login.php">Admin Login</a></li>
        <li><a id="Adminislog" href="view_users.php">AdminHomepage</a></li>
        <li><a href="logout.php"> Logout</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="login.php">Login</a></li>
        <li><a href="registration.php">Register</a></li>
        <li><a href="admin_login.php">Admin Login</a></li>
        <li><a href="logout.php"> Logout</a></li>
      </ul>
    </div>
  </nav>
  <div class="content">
    <div class="row">
      <h2 id="ErrorMessage" class="center-align">Welcome and please register to the site</h2>
      <form class="col s6 offset-s3" action="registration.php" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="input-field col s12">
            <input id="user_name" type="text" name="user_name" class="validate">
            <label for="user_name">Username</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="user_pass" type="password" name="user_pass" class="validate">
            <label for="user_pass">Password</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="user_email" type="email" name="user_email" class="validate">
            <label for="user_email">Email</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <select name="user_country">
              <option value="" disabled selected>Choose your option</option>
              <option value="DK">Denmark</option>
              <option value="FI">Finland</option>
              <option value="NO">Norway</option>
              <option value="SE">Sweden</option>
            </select>
            <label>Country</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="user_no" type="text" name="user_no" class="validate">
            <label for="user_no">Phonenumber</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <textarea id="user_address" name="user_address" class="materialize-textarea"></textarea>
            <label for="user_address">Adress</label>
          </div>
        </div>
        <div class="row">
          <label>Gender</label>
          <p>
            <input name="user_gender" type="radio" id="user_gender_male" value="Male"/>
            <label for="user_gender_male">Male</label>
          </p>
          <p>
            <input name="user_gender" type="radio" id="user_gender_female" value="Femail" />
            <label for="user_gender_female">Female</label>
          </p>
        </div>
        <div class="row">
          <label for="user_bdate">Birthdate</label>
          <input id="user_bdate" type="date" class="datepicker" name="user_bdate">
        </div>
        <div class="row">
          <div class="file-field input-field">
            <div class="btn">
              <span>Image</span>
              <input type="file" name="user_image" class="validate">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" placeholder="only jpg or png">
            </div>
          </div>
        </div>
        <button class="btn waves-effect waves-light" type="submit" name="register">Register Now
          <i class="material-icons right">send</i>
        </button>
      </form>
    </div>
    <?php
    if (isset($_POST['register'])) {
      // getting text info
      $user_name = mysqli_real_escape_string($con,$_POST['user_name']);
      $user_pass = md5(mysqli_real_escape_string($con,$_POST['user_pass']));
      $user_email = mysqli_real_escape_string($con,$_POST['user_email']);
      if (isset($_POST['user_gender']) && isset($_POST['user_country'])) {
        $user_country = mysqli_real_escape_string($con,$_POST['user_country']);
        $user_gender = mysqli_real_escape_string($con,$_POST['user_gender']);
      }
      $user_no = mysqli_real_escape_string($con,$_POST['user_no']);
      $user_address = mysqli_real_escape_string($con,$_POST['user_address']);
      $user_bdate = mysqli_real_escape_string($con,$_POST['user_bdate']);

      // getting image detail
      $user_image = $_FILES['user_image']['name'];
      $user_tmp = $_FILES['user_image']['tmp_name'];
      $imageFileType = pathinfo($user_image,PATHINFO_EXTENSION);
      $user_image = microtime() . "." . $imageFileType;



      // check that all the fileds are filled
      if ($user_address == '' or $user_country == '' or $user_image == '' or $user_gender == '') {
        echo "<script>fillall();</script>";
        exit();
      }
      // Allow certain file formats
      if($imageFileType != "jpg" && $imageFileType != "png") {
        echo "<script>imagecheck();</script>";
        exit();
      }
      // check so the password is longer then 8 characters
      if (strlen($user_pass)< 3) {
        echo "<script>passlen();</script>";
        exit();
      }
      // compare email so not more then one of the same is registerd
      $sel_email = "select * from register_user where user_email = '$user_email'";
      $run_email = mysqli_query($con,$sel_email);
      if (!$run_email) {
        die("Select failed");
      }
      $check_email = mysqli_num_rows($run_email);

      if (!$check_email == 0) {
        echo "<script>emareg();</script>";
        exit();
      }
      // compare username so not more then one of the same is registerd
      $sel_name = "select * from register_user where user_name = '$user_name'";
      $run_name = mysqli_query($con,$sel_name);
      if (!$run_name) {
        die("Select failed");
      }
      $check_name = mysqli_num_rows($run_name);

      if (!$check_name == 0) {
        echo "<script>usereg();</script>";
        exit();
      }
      // if none of the above need to run we insert into database
      else {
        $_SESSION['user_email']=$user_email;

        move_uploaded_file($user_tmp, "images/$user_image");
        $insert = "insert into register_user (user_name, user_pass, user_email, user_country, user_no, user_address, user_gender, user_bdate, user_image, register_date) values ('$user_name', '$user_pass', '$user_email', '$user_country', '$user_no', '$user_address', '$user_gender', '$user_bdate', '$user_image', NOW())";
        $run_insert = mysqli_query($con, $insert);
        if ($run_insert) {
          echo "<script>regsuc();</script>";
          echo "$user_bdate";
          echo "<script>window.open('home.php','_self')</script>";
        }
      }
    }
    ?>

  </div>
  <footer class="page-footer cyan darken-3">
    <div class="footer-copyright">
      <div class="container">
        © 2016 Copyright
        <a class="grey-text text-lighten-4 right" href="mailto:sefo1300@student.miun.se">sefo1300@student.miun.se</a>
      </div>
    </div>
  </footer>
</body>
</html>

<?php

?>
